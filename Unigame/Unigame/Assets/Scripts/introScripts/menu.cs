﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour {

    public void HideMainMenu()
    {
        gameObject.SetActive(false);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void ShowMainMenu()
    {
        gameObject.SetActive(true);
    }
    public void LoadGame(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
