﻿using UnityEngine;
using System.Collections;

public class MenuChickenAnimation : MonoBehaviour {
    /*długość trwania skoku*/
    public float timeTakenDuringMove = 0.1f;

    
    /*długość przemieszczenia - NIE ZMIENIAC*/
    private float distanceToMove = 1.0f;

    /*Czy obiekt jest w ruchu*/
    private bool isMoving = false;

    /*Czas, w którym obiekt zaczął się ruszać*/
    private float timeStartedMoving;

    /*Tag obiektu, który zabił playera*/
    public string trapTag;

    /*czy obiekt żyje*/
    public bool isAlive = true;

   
    /*model kurczaka*/
    public GameObject chicken;

    /*referencja do kamery - drżenie obrazu przy eksplozji miny*/
    public GameObject mainCamera;
    /*========================================================================================================*/

    /*Wektory opisujące zmianę pozycji*/
    private Vector3 startPosition;
    private Vector3 endPosition;
    private Vector3 positionOffset;
    private Vector3 rotationTarget;
    private int frameCnt;
    private int choice;

   




    void Start()
    {
       
        choice = 0;
        frameCnt = 0;
    }

    void startMotion()
    {
        isMoving = true;
        timeStartedMoving = Time.time;
        startPosition = gameObject.transform.position;
        endPosition = gameObject.transform.position + positionOffset;
        AudioSource tmp = gameObject.GetComponent<AudioSource>();
        tmp.Play();

    }

    void LateUpdate()
    {
        /*sprawdzenie, czy gracz jest żywy, jeśli nie, to następuje sprawdzenie
          co konkretnie zabiło gracza, pozwala to na włączenie odpowiedniej
          animacji*/

            frameCnt++;
        if (frameCnt % 100 == 0)
        {
           

            switch (choice)
            {
                case 0:
                    positionOffset = new Vector3(distanceToMove, 0.0f, 0.0f);
                    rotationTarget = Vector3.forward;
                    startMotion();
                    break;
                case 1:
                    positionOffset = new Vector3(-distanceToMove, 0.0f, 0.0f);
                    rotationTarget = Vector3.back;
                    startMotion();
                    break;
                case 2:
                    positionOffset = new Vector3(0.0f, 0.0f, distanceToMove);
                    rotationTarget = Vector3.left;
                    startMotion();
                    break;
                case 3:
                    positionOffset = new Vector3(0.0f, 0.0f, -distanceToMove);
                    rotationTarget = Vector3.right;
                    startMotion();
                    break;

            }
            Debug.Log("frame");
            Debug.Log(frameCnt);
            choice++;
            if (choice > 3) choice = 0;

        }
      
    }

    void FixedUpdate()
    {
        if (isMoving)
        {
            float timeSinceStarted = Time.time - timeStartedMoving;
            float moveInPercentage = timeSinceStarted / timeTakenDuringMove;

            /*Interpolacja pozycji poziomej (na płaszczyźnie)*/
            gameObject.transform.position = Vector3.Lerp(startPosition, endPosition, moveInPercentage);
            if (moveInPercentage >= 1.0f)
            {
                isMoving = false;

                /*gdy moveInPercentage przekracza 1.0f*/
                moveInPercentage = 1.0f;
            }
            /*obliczenie pozycji pionowej (skok)*/
            Vector3 jumpVector = new Vector3(0.0f, 0.5f * Mathf.Sin(Mathf.PI * moveInPercentage), 0.0f);
            gameObject.transform.position += jumpVector;
            Quaternion lookRotation = Quaternion.LookRotation(rotationTarget);
            gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, lookRotation, moveInPercentage);
        }
        /*ustawienie warunku przejścia pomiędzy animacjami*/
    }

    
}
