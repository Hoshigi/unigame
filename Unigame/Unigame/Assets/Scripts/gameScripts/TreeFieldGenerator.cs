﻿using UnityEngine;
using System.Collections.Generic;

public class TreeFieldGenerator : MonoBehaviour {

    public GameObject tree1;
    public GameObject tree2;
    public GameObject stone;
    public GameObject mine;

    /*na jakim przedziale Z mają być generowane przeszkody
     (-range, range)*/
    public int range = 7;

    private List<GameObject> obstacles = new List<GameObject>(0);

	void Start () {

        /*Ile przeszkód ma się znaleźć na trapie*/
        int numberOfObstacles = Random.Range(1, range);

        /*zapełnianie listy referencjami do przeszkód
          oraz losowanie pozycji przeszkód*/
        for (int i = 0; i < numberOfObstacles; i++)
        {
            bool obstacleSet = false;
            do
            {
                float positionZ = (float)Random.Range(-range, range + 1);
                /*sprawdzanie, czy nie wylosowano pozycji
                  dla kilku tych samych przeszkód*/
                if (obstacles.Find(obj => obj.transform.position.z == positionZ))
                    continue;

                /*Jeśli pozycja jest unikalna, to
                  zainicjalizuj przeszkodę*/
                GameObject tmpObstacle;
                switch (Random.Range(0, 4))
                {
                    case 0:
                        tmpObstacle = Instantiate(   tree1,
                                                    new Vector3(gameObject.transform.position.x, 1.0f, positionZ),
                                                    Quaternion.Euler(0.0f, 0.0f, 0.0f)  ) as GameObject;
                        obstacles.Add(tmpObstacle);
                        obstacleSet = true;
                        break;
                    case 1:
                        tmpObstacle = Instantiate(   tree2,
                                                    new Vector3(gameObject.transform.position.x, 1.0f, positionZ),
                                                    Quaternion.Euler(0.0f, 0.0f, 0.0f)  ) as GameObject;
                        obstacles.Add(tmpObstacle);
                        obstacleSet = true;
                        break;
                    case 2:
                        tmpObstacle = Instantiate(   stone,
                                                    new Vector3(gameObject.transform.position.x, 0.1f, positionZ),
                                                    Quaternion.Euler(0.0f, 0.0f, 0.0f)  ) as GameObject;
                        obstacles.Add(tmpObstacle);
                        obstacleSet = true;
                        break;
                    case 3:
                        tmpObstacle = Instantiate(  mine,
                                                    new Vector3(gameObject.transform.position.x, 0.0f, positionZ),
                                                    Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
                        obstacles.Add(tmpObstacle);
                        obstacleSet = true;
                        break;
                }
            } while (!obstacleSet);
        }

	}

    /*
     * Metoda, która jest wywoływana z MapGeneratora,
     * Gdy niszczona jest pułapka
     */
    void OnDestroy()
    {
        for (int i = 0; i < obstacles.Count; i++)
            Destroy(obstacles[i]);
        obstacles.Clear();
    }
}