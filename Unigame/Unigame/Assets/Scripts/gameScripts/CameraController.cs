﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public GameObject player;
    private PlayerController playerData;

    public float minimumDistance = -7.0f;

    /*współczynnik dla kamery podążającej
      wolno za playerem*/
    private float slowCameraFactor = 0.09f;

    /*współczynnik dla kamery goniącej
     playera*/
    private float fastCameraFactor = 3.0f;

    /*różnica odległości, przy jakiej
      kamera zaczyna gonić gracza*/
    private float lengthOffset = 1.0f;

	void Start () {
        playerData = player.GetComponent<PlayerController>();
    }

    void Update()
    {
        /*jeżeli player zaczyna uciekać kamerze;
         jak najszybsza interpolacja do playera*/
        if ((player.transform.position.x - gameObject.transform.position.x > lengthOffset) || !playerData.isAlive)
        {
            Vector3 position = Vector3.Lerp(    gameObject.transform.position,
                                                player.transform.position,
                                                fastCameraFactor * Time.deltaTime  );
            gameObject.transform.position = new Vector3(position.x, 0.0f, 0.0f);
        }
        /*jeżeli player stoi w miejscu albo się cofa;
          powolna interpolacja do punktu umieszczonego
          daleko przed playerem (tak aby, finalnie
          kamera mogła go pokryć)*/
        else
        {
            Vector3 position = Vector3.Lerp(    gameObject.transform.position,
                                                player.transform.position + new Vector3(20.0f, 0.0f, 0.0f),
                                                slowCameraFactor * Time.deltaTime  );
            gameObject.transform.position = new Vector3(position.x, 0.0f, 0.0f);
        }
        /*jeśli kamera jest zbyt blisko playera,
          to player zostaje zabity*/
        if (player.transform.position.x - gameObject.transform.position.x <= minimumDistance)
        {
            PlayerController tmp = player.GetComponent<PlayerController>();
            /*gracz zostaje zabity*/
            if (tmp.isAlive)
            {
                tmp.isAlive = false;
                /*gracz otrzymuje tag kamery (rozpoznaje co go zabiło)*/
                tmp.trapTag = gameObject.tag;
            }
        }
    }
}
