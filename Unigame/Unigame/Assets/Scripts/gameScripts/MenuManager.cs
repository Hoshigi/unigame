﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManager : MonoBehaviour {

    public GameObject gameOverText;
    public GameObject restartText;
    public GameObject menuText;
    public Text score;
    public PlayerController player;
    private MapGenerator scoreRef;
    private bool test = false;

    IEnumerator gameOverMenu()
    {
        gameOverText.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        restartText.SetActive(true);
        menuText.SetActive(true);
        test = true;
    }
	void Start ()
    {
        scoreRef = gameObject.GetComponent<MapGenerator>();
        gameOverText.SetActive(false);
        restartText.SetActive(false);
        menuText.SetActive(false);
	}

	void Update ()
    {
        score.text = "Score: " + (scoreRef.score - 1).ToString();
        if (!player.isAlive && !test)
            StartCoroutine(gameOverMenu());
        if (test)
        {
            if (Input.GetKey(KeyCode.Space))
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            else if (Input.GetKey(KeyCode.Escape))
                SceneManager.LoadScene("menu");
        }
	}
}
