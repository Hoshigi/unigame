﻿using UnityEngine;
using System.Collections;

public class BorderFilterCameraFollow : MonoBehaviour {

    public Transform mainCamera;

	void Update ()
    {
        float posX = mainCamera.position.x;
        gameObject.transform.position = new Vector3(posX, 0.0f, 0.0f);
	}
}
