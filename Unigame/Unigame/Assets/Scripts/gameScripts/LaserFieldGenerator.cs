﻿using UnityEngine;
using System.Collections;

public class LaserFieldGenerator : MonoBehaviour {


    public GameObject warningLight1;
    public GameObject warningLight2;
    public GameObject laserBeam;
    public Renderer light1;
    public Renderer light2;
    private Color lightsColor;
    public int probabilityRange;
    private AudioSource[] laserSounds;

    private int probability;
    private bool test;
	void Start () {
        warningLight1.SetActive(false);
        warningLight2.SetActive(false);
        laserBeam.SetActive(false);
        lightsColor = light1.material.color;
        probability = Random.Range(0, probabilityRange);
        laserSounds = gameObject.GetComponents<AudioSource>();

        StartCoroutine(laserAnimation());

	}

	void Update () {
	}

    IEnumerator laserAnimation()
    {
        while (true)
        {
            if (Random.Range(0, probabilityRange) == probability)
            {
                for (int i = 0; i < 3; i++)
                {
                    laserSounds[0].Play();
                    warningLight1.SetActive(true);
                    warningLight2.SetActive(true);
                    light1.material.color = Color.red;
                    light2.material.color = Color.red;
                    yield return new WaitForSeconds(0.1f);
                    warningLight1.SetActive(false);
                    warningLight2.SetActive(false);
                    light1.material.color = lightsColor;
                    light2.material.color = lightsColor;
                    yield return new WaitForSeconds(0.1f);
                }
                laserSounds[1].Play();
                laserBeam.SetActive(true);
                yield return new WaitForSeconds(1.0f);
                laserBeam.SetActive(false);
            }
            yield return null;
        }
    }
}
