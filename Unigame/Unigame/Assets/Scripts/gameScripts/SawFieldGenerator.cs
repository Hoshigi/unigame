﻿using UnityEngine;
using System.Collections.Generic;

public class SawFieldGenerator : MonoBehaviour {

    public GameObject saw;

    /*prędkość obrotu pił*/
    public float rotateSpeed;

    /*prędkość poruszania się pił*/
    public float minSpeed;
    public float maxSpeed;
    private float speed;

    /*kierunek podążania i rozstaw pił*/
    public bool direction;
    public float minSpace;
    public float maxSpace;


    /*wartości opisujące zmianę pozycji*/
    private float startPositionZ;
    private float endPositionZ;
    private float nextPositionOffset;

    /*lista przechowująca referencje
      do wygenerowanych pił*/
    private List<GameObject> saws = new List<GameObject>(0);


    void Start() {
        
        speed = Random.Range(minSpeed, maxSpeed);
        nextPositionOffset = (float)(Random.Range(minSpace, maxSpace));
        float tmpOffset;

        if (direction) {
            startPositionZ = 15.0f;
            endPositionZ = -15.0f;
            tmpOffset = startPositionZ;
        }
        else {
            startPositionZ = -15.0f;
            endPositionZ = 15.0f;
            tmpOffset = startPositionZ;
        }
        for (int i = 0; i < 15; i++) {

            Vector3 position = gameObject.transform.position + new Vector3(0.0f, 0.0f, tmpOffset);
            GameObject tmp = Instantiate(saw, position, Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
            saws.Add(tmp);

            /*ustalenie odległości od poprzedniej piły*/
            float deltaOffset = (float)Random.Range(minSpace, maxSpace);
            if (direction)
                tmpOffset -= deltaOffset;
            else
                tmpOffset += deltaOffset;
        }
        
    }

    void FixedUpdate() {

        for (int i = 0; i < saws.Count; i++)
            saws[i].transform.Rotate(Vector3.right, 40.0f);

        if (direction) {
            for (int i = 0; i < saws.Count; i++)
                saws[i].transform.position -= new Vector3(0.0f, 0.0f, speed * Time.deltaTime);
            if (saws[saws.Count - 1].transform.position.z <= endPositionZ) {
                Destroy(saws[saws.Count - 1]);
                saws.RemoveAt(saws.Count - 1);
            }
        } 
        else {
            for (int i = 0; i < saws.Count; i++)
                saws[i].transform.position += new Vector3(0.0f, 0.0f, speed * Time.deltaTime);
            if (saws[saws.Count - 1].transform.position.z >= endPositionZ) {
                Destroy(saws[saws.Count - 1]);
                saws.RemoveAt(saws.Count - 1);
            }
        }
        if (Mathf.Abs(startPositionZ - saws[0].transform.position.z) >= nextPositionOffset) {
            Vector3 position = gameObject.transform.position + new Vector3(0.0f, 0.0f, startPositionZ);
            GameObject tmp = Instantiate(saw, position, Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
            saws.Insert(0, tmp);
            nextPositionOffset = (float)(Random.Range(minSpace, maxSpace));
        }
    }

    void OnDestroy() {
        for (int i = 0; i < saws.Count; i++)
            Destroy(saws[i]);
        saws.Clear();
    }
}
