﻿using UnityEngine;
using System.Collections;

public class mineController : MonoBehaviour {

    //private bool mineIsActive;

	void Start () {
        //mineIsActive = true;
	}
    void OnCollisionEnter(Collision col)
    {
        if (col.collider.CompareTag("Player"))
        {
            GameObject player = col.collider.gameObject;
            PlayerController tmp = player.GetComponent<PlayerController>();
            if (tmp.isAlive)
            {
                tmp.isAlive = false;
                tmp.trapTag = gameObject.tag;
            }
            //mineIsActive = false;
            gameObject.SetActive(false);
        }
    }
}
