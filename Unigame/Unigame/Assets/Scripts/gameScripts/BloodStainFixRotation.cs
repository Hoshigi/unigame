﻿using UnityEngine;
using System.Collections;


public class BloodStainFixRotation : MonoBehaviour {

    public GameObject parent;
    void Start()
    {
        gameObject.transform.rotation = gameObject.transform.rotation * parent.transform.rotation;
    }

}
