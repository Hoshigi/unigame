﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MapGenerator : MonoBehaviour {

    /*pole z trawą, na której rosną drzewa */
    public GameObject treeField;

    /*pole, na którym jest tylko trawa*/
    public GameObject emptyField;

    /*pole z piłą tarczową*/
    public GameObject sawField;

    /*pole z laserem*/
    public GameObject laserField;

    /*referencja do playera*/
    public PlayerController player;


    /*liczba pułapek generowanych na starcie*/
    public int numberOfFieldsOnStart = 20;

    /*Ilość punktów*/
    public int score = 0;

    /*lista pułapek*/
    private List<GameObject> traps = new List<GameObject>(0);

    /* jeśli pozycja x playera wyjdzie poza tę wartość
     * dodawana jest pułapka na pozycji x == trapPositionMax,
     * a trapPositionMax jest zwiększane o 1.0f*/
    private float trapPositionMax = -25.0f;

	void Start () {
        GameObject trap;

        while (trapPositionMax < 0.0f)
        {
            trap = Instantiate( emptyField,
                                new Vector3(trapPositionMax, -0.1f, 0.0f),
                                Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
            traps.Add(trap);
            trapPositionMax += 1.0f;
        }
        trap = Instantiate( emptyField,
                            new Vector3(trapPositionMax, -0.1f, 0.0f),
                            Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
        traps.Add(trap);

        for (int i = 0; i < numberOfFieldsOnStart - 1; i++)
            GenerateField();

	}
	
	void Update () {

        /*generowanie kolejnych pułapek z
         każdym NOWYM krokiem do przodu*/
        if ((player.transform.position.x + (float)numberOfFieldsOnStart) > trapPositionMax)
        {
            GenerateField();
            score++;
        }
	}

    void GenerateField()
    {
        trapPositionMax += 1.0f;
        /*Losowanie pułapki*/
        if (traps[traps.Count - 1].name == "TreeField(Clone)") //<---------------------------------------------------   jak zmieniasz nazwę obiektu
                                                                                                                      //to zmień również tutaj
        {
            GameObject trap = Instantiate(  emptyField,
                                            new Vector3(trapPositionMax, -0.1f, 0.0f),
                                            Quaternion.Euler(0.0f, 0.0f, 0.0f)  ) as GameObject;
            traps.Add(trap);
        }
        else
        {
            GameObject trap;
            switch (Random.Range(0, 4))
            {
                case 0:
                    trap = Instantiate( emptyField,
                                        new Vector3(trapPositionMax, -0.1f, 0.0f),
                                        Quaternion.Euler(0.0f, 0.0f, 0.0f)  ) as GameObject;
                    traps.Add(trap);
                    break;
                case 1:
                    trap = Instantiate( treeField,
                                        new Vector3(trapPositionMax, -0.1f, 0.0f),
                                        Quaternion.Euler(0.0f, 0.0f, 0.0f)  ) as GameObject;
                    traps.Add(trap);
                    break;
                case 2:
                    trap = Instantiate( sawField,
                                        new Vector3(trapPositionMax, 0.0f, 0.0f),
                                        Quaternion.Euler(0.0f, 0.0f, 0.0f)  ) as GameObject;
                    traps.Add(trap);
                    SawFieldGenerator tmpObject = traps[traps.Count - 1].GetComponent<SawFieldGenerator>();
                    tmpObject.direction = (Random.value > 0.5f);
                    break;
                case 3:
                    trap = Instantiate(laserField,
                                        new Vector3(trapPositionMax, 0.0f, 0.0f),
                                        Quaternion.Euler(0.0f, 0.0f, 0.0f)) as GameObject;
                    traps.Add(trap);
                    break;
            }
        }

        /*jeśli player przejdzie odpowiednią liczbę pułapek,
         zbędne pułapki za nim są usuwane*/
        if ((player.transform.position.x > numberOfFieldsOnStart))
        {
            Destroy(traps[0]);
            traps.RemoveAt(0);
        }
    }
}
