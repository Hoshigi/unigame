﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
    
    /*długość trwania skoku*/
    public float timeTakenDuringMove = 0.1f;

    /*długość przemieszczenia - NIE ZMIENIAC*/
    private float distanceToMove = 1.0f;

    /*Czy obiekt jest w ruchu*/
    private bool isMoving = false;

    /*Czas, w którym obiekt zaczął się ruszać*/
    private float timeStartedMoving;

    /*Tag obiektu, który zabił playera*/
    public string trapTag;

    /*czy obiekt żyje*/
    public bool isAlive = true;

    /*========================================================================================================*/
    /*obiekt zawierający światła i cząsteczki
      dla wybuchu miny*/
    public GameObject mineExplosionObject;

    /*obiekt zawierający światła i cząsteczki
      dla spalenia laserem*/
    public GameObject laserExplosionObject;

    /*obiekt zawierający światła i cząsteczki
      dla uderzenia piły*/
    public GameObject sawHitObject;

    /*model kurczaka*/
    public GameObject chicken;

    /*referencja do kamery - drżenie obrazu przy eksplozji miny*/
    public GameObject mainCamera;
    /*========================================================================================================*/

    /*Wektory opisujące zmianę pozycji*/
    private Vector3 startPosition;
    private Vector3 endPosition;
    private Vector3 positionOffset;
    private Vector3 rotationTarget;

    IEnumerator mineExplosion()
    {
        chicken.SetActive(false);
        GameObject ExplosionHole = mineExplosionObject.transform.Find("ExplosionHole").gameObject;
        GameObject ExplosionLight = mineExplosionObject.transform.Find("ExplosionLight").gameObject;
        GameObject Particles = mineExplosionObject.transform.Find("Particles").gameObject;
        AudioSource explosionSound = mineExplosionObject.GetComponent<AudioSource>();

        explosionSound.Play();
        ExplosionHole.SetActive(true);
        Particles.SetActive(true);
        ExplosionLight.SetActive(true);
        mainCamera.transform.Translate(0.0f, 0.0f, 2.0f);
        yield return new WaitForSeconds(0.1f);
        mainCamera.transform.Translate(0.0f, 0.0f, -2.0f);
        ExplosionLight.SetActive(false);

        yield return null;
    }
    IEnumerator laserExplosion()
    {
        chicken.SetActive(false);
        GameObject BurnedDust = laserExplosionObject.transform.Find("BurnedDust").gameObject;
        GameObject ExplosionLight = laserExplosionObject.transform.Find("ExplosionLight").gameObject;
        GameObject Particles = laserExplosionObject.transform.Find("Particles").gameObject;
        AudioSource ElectricSparksSound = laserExplosionObject.GetComponent<AudioSource>();

        ElectricSparksSound.Play();
        BurnedDust.SetActive(true);
        Particles.SetActive(true);
        ExplosionLight.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        ExplosionLight.SetActive(false);

        yield return null;
    }
    IEnumerator sawHit()
    {
        chicken.SetActive(false);
        GameObject BloodStain = sawHitObject.transform.Find("BloodStain").gameObject;
        GameObject Particles = sawHitObject.transform.Find("Particles").gameObject;
        AudioSource bloodSplatterSound = sawHitObject.GetComponent<AudioSource>();

        bloodSplatterSound.Play();
        BloodStain.SetActive(true);
        Particles.SetActive(true);
        yield return null;
    }
    void Start()
    {
    }

    void startMotion()
    {
        isMoving = true;
        timeStartedMoving = Time.time;
        startPosition = gameObject.transform.position;
        endPosition = gameObject.transform.position + positionOffset;
        AudioSource tmp = gameObject.GetComponent<AudioSource>();
        tmp.Play();
    }

    void LateUpdate()
    {
        /*sprawdzenie, czy gracz jest żywy, jeśli nie, to następuje sprawdzenie
          co konkretnie zabiło gracza, pozwala to na włączenie odpowiedniej
          animacji*/
        if (!isAlive)
        {
            Collider gameObjectCollider = gameObject.GetComponent<Collider>();
            /*sprawdzenie, co zabiło gracza*/
            switch (trapTag)
            {
                case "saw":
                    Debug.Log("saw killed player");
                    StartCoroutine(sawHit());
                    gameObjectCollider.enabled = false;
                    trapTag = "";
                    break;
                case "MainCamera":
                    Debug.Log("camera killed player");
                    break;
                case "LaserBeam":
                    Debug.Log("LaserBeam killed player");
                    StartCoroutine(laserExplosion());
                    gameObjectCollider.enabled = false;
                    trapTag = "";
                    break;
                case "mine":
                    Debug.Log("mine killed player");
                    StartCoroutine(mineExplosion());
                    gameObjectCollider.enabled = false;
                    trapTag = "";
                    break;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) && !isMoving)
            {
                positionOffset = new Vector3(distanceToMove, 0.0f, 0.0f);
                rotationTarget = Vector3.forward;
                CheckForCollisions();
            }
            if (Input.GetKeyDown(KeyCode.DownArrow) && !isMoving)
            {
                positionOffset = new Vector3(-distanceToMove, 0.0f, 0.0f);
                rotationTarget = Vector3.back;
                CheckForCollisions();
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) && !isMoving)
            {
                positionOffset = new Vector3(0.0f, 0.0f, distanceToMove);
                rotationTarget = Vector3.left;
                CheckForCollisions();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow) && !isMoving)
            {
                positionOffset = new Vector3(0.0f, 0.0f, -distanceToMove);
                rotationTarget = Vector3.right;
                CheckForCollisions();
            }
        }
    }

    void FixedUpdate()
    {
        if (isMoving)
        {
            float timeSinceStarted = Time.time - timeStartedMoving;
            float moveInPercentage = timeSinceStarted / timeTakenDuringMove;

            /*Interpolacja pozycji poziomej (na płaszczyźnie)*/
            gameObject.transform.position = Vector3.Lerp(startPosition, endPosition, moveInPercentage);
            if (moveInPercentage >= 1.0f)
            {
                isMoving = false;

                /*gdy moveInPercentage przekracza 1.0f*/
                moveInPercentage = 1.0f;
            }
            /*obliczenie pozycji pionowej (skok)*/
            Vector3 jumpVector = new Vector3(0.0f, 0.5f * Mathf.Sin(Mathf.PI * moveInPercentage), 0.0f);
            gameObject.transform.position += jumpVector;
            Quaternion lookRotation = Quaternion.LookRotation(rotationTarget);
            gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, lookRotation, moveInPercentage);
        }
        /*ustawienie warunku przejścia pomiędzy animacjami*/
    }

    void CheckForCollisions()
    {
        /*w tej metodze pod uwagę brane są tylko te kolizje, które wywołał player,
          tzn. jeśli player wpadł na drzewo, lub na BOK piły (jeśli piła najechała
          na playera ostrzem, to ta kolizja jest obsługiwana w skrypcie piły,
          jeśli na playera spadnie meteoryt, albo przebije go kolec, to te kolizje
          też są sprawdzane w skryptach meteorytu i kolców, a nie tutaj
         
          w skrócie: ta metoda zapobiega tylko temu, aby player
          nie wchodził na drzewa, kamienie i inne rzeczy*/

        RaycastHit hitInfo;
        Ray ray = new Ray(gameObject.transform.position, positionOffset);
        if (!Physics.Raycast(ray, out hitInfo, 1.0f))
            startMotion();
        else
        {
            if (hitInfo.collider.CompareTag("LaserBeam"))
                startMotion();
            if (hitInfo.collider.CompareTag("mine"))
                startMotion();
        }

        /*w oknie edytora rysuje czerwony promień kolizji*/
        Debug.DrawRay(gameObject.transform.position, positionOffset, Color.red, 0.1f);
    }
}
